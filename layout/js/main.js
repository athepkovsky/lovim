Modernizr.load([{
    load: 'http://api-maps.yandex.ru/2.1/?load=Map&lang=ru_RU',
    complete: function () {
        var myMap;
        ymaps.ready(function () {
            myMap = new ymaps.Map("map", {
                center: [55.76, 37.64],
                zoom: 10
            });
        });        
    }
}]);

(function($) {
	$(function() {
		// Tabs
		$(document).on('click', '.found-links a', function() {
			var id = $(this).attr('href');
			if (!$(id).hasClass('active')) {
				$('.search-tabs li').removeClass('active');
				$('.search-tabs a[href="' + id + '"]').parent().addClass('active');
			}
		});

		// Locator Block
		$('#search-form').on('submit', function(e) {
			e.preventDefault();
			$('.locator-closed').fadeOut(200, function() {
				$('.map-container .container').addClass('locator-bg');
				$('.locator-opened').fadeIn(200, function() {
					var $_this = $(this);
					$(document).on('click', function(e) {
  						if (!$(e.target).closest('.map-container .container').length) {
    						$_this.fadeOut(200, function() {
    							$('.map-container .container').removeClass('locator-bg');
								$('.locator-closed').fadeIn(200);
							});
						}
					});
				});
			});
		});
	});
})(jQuery);
